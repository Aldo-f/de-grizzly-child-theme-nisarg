jQuery(document).ready(function () {
// add class to table
jQuery("table").addClass("table table-striped sortable");

// delete width
jQuery("td").attr({"width":""});


// change all <td> in :first <tr> to <th>

jQuery("table tr:first td").each(function() {
  jQuery(this).replaceWith('<th>' + jQuery(this).text() + '</th>'); 
 });

jQuery("<thead></thead>").prependTo("table").append(jQuery("table tr:first"));
});
