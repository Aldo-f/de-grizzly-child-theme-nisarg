<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package Nisarg
 */

?>

	</div><!-- #content -->
	
	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="row site-info">
			<?php echo '&copy; 2015-' .date("Y"); ?> 
			<?php echo 'Els & The Artists'; ?>
			<span class="sep"> | </span>
			<?php echo 'Made with <span class="animated infinite pulse">♥</span> by <a href="https://fb.com/aldo.fieuw">Aldo</a> '; ?> 
		</div><!-- .site-info -->
	</footer><!-- #colophon -->
</div><!-- #page -->
<?php wp_footer(); ?>
</body>
</html>
